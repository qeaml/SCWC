# SCWC
See the vocabulary behind your code!

# What does it do?
SCWC (short for source code word count) looks through any folder you give it, and reads through every file with a given extension. When it's reading through, it keeps track of every word in the file, but not just a naive `.split(' ')`. It can separate words out of `hello_world`, `helloWorld` and `hello.world`. Once it has done that, the CLI will throw some statistics at you (for each file). These statistics include:
* Average count of each word
* Average length of the words
* The least common words
* The amount the least common words occured
* The most common words
* The amount the most common words occured
* The longest word and it's length
* The shortest word and it's length

# How do I use it?
At the moment, you'll need to:
1. `git clone` this repo;
2. Navigate into the repo's folder;
3. Use `py . (folder name) (extension)`;
    The folder name is recommended to be a full path, and the extension is self-explainatory.
    Example: `py . dummy_code py`

# Why does it exist?
It's just something I had in the back of my head for some time, and I finally got the motivation to actually make it a thing. I was also quite curious about the code I write. 

*Fun fact:* according to this "tool", the longest word that appears in the screeptist codebase(as of 0.0.5) is: **representation**, with a whopping length of **14** characters! The next longest words are: *screeptist, represents, stringlike, expression, endroutine and statements*, with a length of 10 characters.