import word_detector as _wd

def prettify(text: str) -> str:
    tmp = _wd.detectWords(text)
    tmp[0] = tmp[0].capitalize()
    return ' '.join(tmp)