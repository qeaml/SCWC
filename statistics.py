import dataclasses as _dc

@_dc.dataclass
class Stats:
    mostCommon:     list  
    mostCommonAmt:  int   
    leastCommon:    list  
    leastCommonAmt: int   
    longest:        str   
    longestLen:     int   
    shortest:       str   
    shortestLen:    int   
    avgCount:       float 
    avgLen:         float 

def analyze(inp: dict) -> Stats:
    mostCommon:     list  = []
    mostCommonAmt:  int   = 0
    leastCommon:    list  = []
    leastCommonAmt: int   = 0
    longest:        str   = []
    longestLen:     int   = 0
    shortest:       str   = []
    shortestLen:    int   = 0
    avgCount:       float = 0.0
    avgLen:         float = 0.0

    countTotalVal  = 0
    countTotalCnt  = 0
    lengthTotalVal = 0

    for word,count in inp.items():
        countTotalVal += count
        countTotalCnt += 1

        if count > mostCommonAmt:
            mostCommonAmt = count
            mostCommon = [word]
        elif count == mostCommonAmt:
            mostCommon += [word]

        length = len(word)
        lengthTotalVal += length
        if length > longestLen:
            longestLen = length
            longest = [word]
        elif length == longestLen:
            longest += [word]

    avgCount = countTotalVal  / countTotalCnt
    avgLen   = lengthTotalVal / countTotalCnt
    
    leastCommon    = mostCommon
    leastCommonAmt = mostCommonAmt
    shortest       = longest
    shortestLen    = longestLen
    for word,count in inp.items():
        if count < leastCommonAmt:
            leastCommonAmt = count
            leastCommon = [word]
        elif count == leastCommonAmt:
            leastCommon += [word]
        
        length = len(word)
        if length < shortestLen:
            shortestLen = length
            shortest = [word]
        elif length == shortestLen:
            shortest += [word]

    return Stats(
        mostCommon, mostCommonAmt, leastCommon, leastCommonAmt,
        longest, longestLen, shortest, shortestLen,
        avgCount, avgLen
    )

if __name__ == '__main__':
    import code_indexer
    folder = code_indexer.getFolderWords("dummy_code", "py")
    hello: dict = folder["hello.py"]
    print(analyze(hello))