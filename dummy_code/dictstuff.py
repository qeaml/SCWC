d = {"hello":"world","foo":"bar"}

# interate through keys
for k in d.keys(): 
    pass

# iterate through values
for v in d.values():
    pass

# iterate through (key,value) pairs
for k,v in d.items():
    pass