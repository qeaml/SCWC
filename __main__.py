import sys
import code_indexer
import statistics
import utils
from os import path

if len(sys.argv) > 2:
    folder = sys.argv[1]
    extension = sys.argv[2]
    if not path.isdir(folder):
        print("specified path isn't a folder or doesn't exist")
        exit(1)
    
    # check if index isn't empty
    if not (index := code_indexer.getFolderWords(folder, extension)):
        print(
            "no files with that extension were found. "
            "are you sure the path and extension are correct?"
        )
    for file,words in index.items():
        print(f"Statistics for {file}: ".ljust(80, "="))
        stats = statistics.analyze(words)
        for attr in dir(stats):
            if not attr.startswith("__"):
                print(f"{utils.prettify(attr).rjust(25)}: {getattr(stats, attr)}")
        print()
else:
    print(f"Usage: {sys.argv[0]} (folder name) (extension)")