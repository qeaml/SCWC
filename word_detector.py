import string as _string

WORDSEP: str = "._" + _string.whitespace

def _toAlphanum(text: str) -> str:
    return ''.join([c for c in text if c.isalnum()])

def _isUpper(ch: str) -> bool:
    return ch.upper() == ch

def _isLower(ch: str) -> bool:
    return ch.lower() == ch

def detectWords(text: str) -> list:
    out = []
    tmp = ""
    for c in text:
        if tmp != "":
            if _isUpper(c) and _isLower(tmp[-1]):
                out += [tmp.lower()]
                tmp = ""
        tmp += c
        if c in WORDSEP:
            out += [tmp.lower()[1:]]
            tmp = ""
    if tmp != "":
        out += [tmp.lower()]
    real_out = []
    for word in out:
        alphanum = _toAlphanum(word).strip()
        if alphanum != "":
            real_out += [alphanum]
    return real_out

if __name__ == '__main__':
    src = "word_detector.detectWords() detects words"
    words = detectWords(src)
    assert words == ['word', 'detector', 'detect', 'words', 'detects', 'words']
