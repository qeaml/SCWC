import os as _os
import word_detector as _wd
import collections as _co

def _getFilesWithExtension(path: str, ext: str) -> list:
    out = []
    for dirpath, _, filenames in _os.walk(path):
        out += [_os.path.join(dirpath, filename) for filename in filenames]
    return [filename for filename in out if filename.endswith(f".{ext}")]

def getFolderWords(path: str, ext: str) -> dict:
    out = dict()
    files = _getFilesWithExtension(path, ext)
    for file in files:
        _, filename = _os.path.split(file)
        out[filename] = _co.defaultdict(int)
        with open(file, "rt") as fh:
            for ln in fh:
                words = _wd.detectWords(ln)
                for word in words:
                    out[filename][word] += 1
    return {fn: dict(dd) for fn,dd in out.items()}

if __name__ == '__main__':
    print(getFolderWords("dummy_code", "py"))
                    